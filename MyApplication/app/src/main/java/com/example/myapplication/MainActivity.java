package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.net.DhcpInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.util.Date;

import kotlin.text.UStringsKt;

public class MainActivity extends AppCompatActivity {

    int udpPort = 1025;
    DatagramSocket socket;

    TextView sensorsDataReceivedTimeTextView;
    TextView temperatureValueTextView;
    TextView avgTemperatureValueTextView;
    Button refreshButton;
    Button startButton;

    boolean appInBackground = false;
    boolean doneEditing = true;

    @Override
    protected void onResume() {
        super.onResume();
        appInBackground = false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        appInBackground = true;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        final Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON); // Turn screen on if off
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON); // Keep screen on

        sensorsDataReceivedTimeTextView = (TextView) findViewById(R.id.wateringDataReceivedTimeTextView);

        temperatureValueTextView = (TextView) findViewById(R.id.temperatureValueTextView);
        avgTemperatureValueTextView = (TextView) findViewById(R.id.avgTemperatureValueTextView);
        refreshButton = (Button) findViewById(R.id.refreshButton);
        startButton = (Button) findViewById(R.id.startButton);



        // request sensors data from WoT sensors node
        (new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    socket = new DatagramSocket(udpPort);
                    while (true) {
                        if (appInBackground) {
                            continue;
                        }
                        try {
                            sendUdpData(Commands.GET_ALL_SENSORS_DATA, null);
                            Thread.sleep(10000);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        })).start();

        // listen for data from requesting data from WoT sensors node
        (new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    if (appInBackground) {
                        continue;
                    }
                    DatagramPacket udpPacket = receiveUdpData();
                    if (udpPacket == null) {
                        continue;
                    }
                    String udpPacketData =  new String( udpPacket.getData());
                    try {
                        JSONObject jsonObj = new JSONObject(udpPacketData);
                        updateUserInterface( jsonObj);
                    } catch ( JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        })).start();

        refreshButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // request data from WoT sensors node
                (new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {

                            avgTemperatureValueTextView.setText("123");
                            sendUdpData( Commands.GET_ALL_SENSORS_DATA, null);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                })).start();
            }
        });

        startButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // request data from WoT sensors node
                (new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {

                            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
                            sensorsDataReceivedTimeTextView.setText(dateFormat.format(new Date()));
                            sendUdpData( Commands.GET_ALL_SENSORS_DATA, null);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                })).start();
            }
        });




    }

    void updateUserInterface( final JSONObject jsonObj) {
        try {
            final double temperature = jsonObj.getDouble("temperature");
            final String heure = jsonObj.getString("heure");

            temperatureValueTextView.post(new Runnable() {
                public void run() {
                    temperatureValueTextView.setText(String.valueOf(temperature).concat("°C"));
                }
            });

            sensorsDataReceivedTimeTextView.post(new Runnable() {
                public void run() {
                    sensorsDataReceivedTimeTextView.setText(heure);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    InetAddress getBroadcastAddress() throws IOException {
        WifiManager wifi = (WifiManager)getSystemService( Context.WIFI_SERVICE);
        DhcpInfo dhcp = wifi.getDhcpInfo();
        // no DHCP info...can't do nothing more
        if ( dhcp == null) {
            return null;
        }
        int ipAddress = dhcp.gateway;
        byte[] ipQuads = new byte[4];
        ipQuads[0] = (byte)(ipAddress & 0xFF);;
        ipQuads[1] = (byte)((ipAddress >> 8) & 0xFF);
        ipQuads[2] = (byte)((ipAddress >> 16) & 0xFF);
        ipQuads[3] = (byte)((ipAddress >> 24) & 0xFF);

        /* 192.168.4.1 = [ -64, -88, 4, 1] */
        return InetAddress.getByAddress( ipQuads);
    }

    void sendUdpData( Commands cmd, byte[] params) {
        try {
            final DatagramPacket packet;
            int paramsLength = ( params != null ? params.length : 0);
            byte data[] = new byte[paramsLength + 1];
            byte command[] = new byte[1];
            command[0] = cmd.getValue();

            System.arraycopy( command, 0, data, 0, command.length);
            if ( params != null) {
                System.arraycopy(params, 0, data, 1, params.length);
            }

            InetAddress serverAddress = InetAddress.getByName("192.168.1.35"); // Remplacez "IP_DU_PC" par l'adresse IP de votre PC
            // DESKTOP-DH86FHK
            // serverAddress = getBroadcastAddress();
            packet = new DatagramPacket( data, data.length,
                    serverAddress, udpPort);
            socket.send( packet);
        } catch( IOException e){
            e.printStackTrace();
        }
    }

    DatagramPacket receiveUdpData() {
        try {
            byte[] data  = new byte[1024];
            DatagramPacket packet = new DatagramPacket( data, data.length);
            if ( socket == null) {
                return null;
            }
            socket.receive(packet);
            Log.i("HD:receiveUdpData", new String( packet.getData()).trim());
            return packet;
        } catch( IOException e){
            Log.e("HD:receiveUdpData", "Error occurred when receiving UDP data on port: " + udpPort);
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Define the set of commands which can be send to the WoT sensor(s) node.
     */
    enum Commands {
        GET_ALL_SENSORS_DATA ( (byte)99);
        private final byte id;
        Commands( byte id) { this.id = id; }
        public byte getValue() { return id; }
    }
}