int pinTemp = A1;   //This is where our Output data goes
int ledPin=8;

void setup() {
  Serial.begin(9600);
  pinMode(ledPin,OUTPUT);
}
void loop() {
  int temp = analogRead(pinTemp);    //Read the analog pin
  temp = temp * 0.48828125;   // convert output (mv) to readable celcius
  Serial.println(temp);
  delay(1000);
  digitalWrite(ledPin,HIGH); //HIGH is set to about 5V PIN8
  delay(1010);            //Set the delay time, 1000 = 1S
  digitalWrite(ledPin,LOW);  //LOW is set to about 5V PIN8
  delay(2000);
}