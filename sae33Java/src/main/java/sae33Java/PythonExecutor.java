package sae33Java;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class PythonExecutor {

    private String pythonScriptPath;

    public PythonExecutor(String pythonScriptPath) {
        this.pythonScriptPath = pythonScriptPath;
    }

    public String executeScript() {
        StringBuilder output = new StringBuilder();

        try {
            // Commande pour exécuter le script Python
            String[] cmd = {"python", pythonScriptPath};

            // Exécute le script Python
            Process process = Runtime.getRuntime().exec(cmd);

            // Lit la sortie standard du script Python
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                output.append(line).append("\n");
            }

            // Attendez que le processus se termine
            int exitCode = process.waitFor();

            // Affiche la sortie du script Python
            System.out.println(output.toString());

            // Affiche le code de sortie du processus
            System.out.println("Code de sortie du processus : " + exitCode);

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }

        return output.toString();
    }
}
