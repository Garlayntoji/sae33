package sae33Java;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ServerMaster {

    private static final int SERVER_PORT = 8080; // Le port sur lequel le serveur écoutera

    public static void main(String[] args) {
        // Initialisation du serveur maître
        ServerMaster server = new ServerMaster();

        // Démarrage du serveur maître
        server.start();

        // ... (Le reste du code)
        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        scheduler.scheduleAtFixedRate(server::serialReader, 0, 1, TimeUnit.HOURS);
        
        String jdbcUrl = "jdbc:mysql://localhost:3306/sae33";
        String user = "root";
        String password = "jksaveto9852";

        DatabaseAccessor dbAccessor = new DatabaseAccessor(jdbcUrl, user, password);
        
        
    }

    public void start() {
        // Logique d'initialisation du serveur maître
        System.out.println("Server Master started.");

        // Initialisation de la partie réseau (exemple basique)
        try {
            ServerSocket serverSocket = new ServerSocket(SERVER_PORT);
            System.out.println("Server listening on port " + SERVER_PORT);

            // Accepter les connexions clients dans un thread séparé
            new Thread(() -> {
                while (true) {
                    try {
                        Socket clientSocket = serverSocket.accept();
                        handleClientConnection(clientSocket);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
            System.out.println("Server is now ready.");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void handleClientConnection(Socket clientSocket) {
        // Logique pour gérer la connexion d'un client
        System.out.println("Client connected: " + clientSocket.getInetAddress());
        // ... (Ajoutez la logique de gestion des communications avec le client)
    }
    public String serialReader() {
    	PythonExecutor reader = new PythonExecutor("../saeSerial/serialReader.py");
    	System.out.println(reader.executeScript());
    	return reader.executeScript();
    }
    public void serialSender() {
    	//TODO
    }
    
    public void tempRegistar(DatabaseAccessor dbAccessor) {
    	String inputTemp = this.serialReader();
    	dbAccessor.registerTemp(Float.parseFloat(inputTemp), 0);
    }
}