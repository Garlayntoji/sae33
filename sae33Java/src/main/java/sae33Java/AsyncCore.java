package sae33Java;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class AsyncCore {
	public static void main(String[] args) throws InterruptedException, ExecutionException {
        PythonExecutor serialReader = new PythonExecutor("../saeSerial/serialReader.py");
		
		CompletableFuture<String> future = CompletableFuture.supplyAsync(() -> {
            return serialReader.executeScript();
        });

        // Attendez la fin de l'exécution asynchrone
        String result = future.get();
        System.out.println(result);
    }
}
