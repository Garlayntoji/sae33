package sae33Java;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

public class DatabaseAccessor {

    private Connection connection;

    // Constructeur : initialisation de la connexion à la base de données
    public DatabaseAccessor(String jdbcUrl, String user, String password) {
        try {
            // Charger le driver JDBC
            Class.forName("com.mysql.cj.jdbc.Driver");

            // Établir la connexion à la base de données
            this.connection = DriverManager.getConnection(jdbcUrl, user, password);

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    // Méthode pour exécuter une requête SELECT
    public void executeSelectQuery(String query) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(query);
             ResultSet resultSet = preparedStatement.executeQuery()) {

            // Traitement des résultats
            while (resultSet.next()) {
                // Lire les données de chaque ligne
                int id = resultSet.getInt("id");
                // Faire quelque chose avec les données...
                System.out.println("ID: " + id);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void selectTempHistory(int arduinoControllerID) {
    	try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM TemperatureHistory  WHERE arduino = "+arduinoControllerID+";");
        	ResultSet resultSet = preparedStatement.executeQuery()) {
    		// "SELECT * FROM TEmperatureHistory WHERE arduino = "+arduinoControllerID+";"
    			while (resultSet.next()) {
    				float temp = resultSet.getFloat("temperature");
                    Timestamp time = resultSet.getTimestamp("timestamp");
                    System.out.println("Temperature: "+temp+" | Date: "+time);
    			}
    		}
    	catch (SQLException e) {
    		e.printStackTrace();
    	}
    }
    
    public void registerTemp(float inputTemp, int arduinoController) {
    	try {
    		PreparedStatement statement = connection.prepareStatement("INSERT INTO TemperatureHistory (temperature, timestamp, arduino) VALUES ("+inputTemp+",CURRENT_TIMESTAMP,"+arduinoController+");");
    		statement.executeQuery();
    	}
    	catch (SQLException e) {
    		e.printStackTrace();
    	}
    }

    // Méthode pour exécuter une requête INSERT, UPDATE ou DELETE
    public int executeUpdateQuery(String query) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {

            // Exécuter la requête
            int rowsAffected = preparedStatement.executeUpdate();

            // Retourner le nombre de lignes affectées
            return rowsAffected;

        } catch (SQLException e) {
            e.printStackTrace();
            return -1; // En cas d'erreur
        }
    }

    // Méthode pour fermer la connexion à la base de données
    public void closeConnection() {
        try {
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        // Exemple d'utilisation de la classe
        String jdbcUrl = "jdbc:mysql://localhost:3306/sae33";
        String user = "root";
        String password = "jksaveto9852";

        DatabaseAccessor dbAccessor = new DatabaseAccessor(jdbcUrl, user, password);

        // Exemple d'une requête SELECT
        String selectQuery = "SELECT * FROM TemperatureHistory";
        dbAccessor.executeSelectQuery(selectQuery);

        // Exemple d'une requête UPDATE
        /*
        String updateQuery = "UPDATE votre_table SET name = 'NouveauNom' WHERE id = 1";
        int rowsAffected = dbAccessor.executeUpdateQuery(updateQuery);
        System.out.println("Nombre de lignes modifiées : " + rowsAffected);
		*/
        // Fermer la connexion à la base de données
        dbAccessor.closeConnection();
    }
}
