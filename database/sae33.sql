-- phpMyAdmin SQL Dump
-- version 5.1.1deb5ubuntu1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:80
-- Généré le : mar. 02 jan. 2024 à 15:17
-- Version du serveur : 10.6.12-MariaDB-0ubuntu0.22.04.1
-- Version de PHP : 8.1.2-1ubuntu2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `sae33`
--

-- --------------------------------------------------------

--
-- Structure de la table `ArduinoController`
--

CREATE TABLE `ArduinoController` (
  `id` int(11) NOT NULL,
  `controller_name` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `TemperatureHistory`
--

CREATE TABLE `TemperatureHistory` (
  `id` int(11) NOT NULL,
  `temperature` float NOT NULL,
  `timestamp` date NOT NULL,
  `arduino` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `WateringHistory`
--

CREATE TABLE `WateringHistory` (
  `id` int(11) NOT NULL,
  `arduino` int(11) NOT NULL,
  `timestamp` date NOT NULL,
  `temperature` int(11) NOT NULL,
  `type` enum('AUTO','MAN') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `ArduinoController`
--
ALTER TABLE `ArduinoController`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `controller_name` (`controller_name`);

--
-- Index pour la table `TemperatureHistory`
--
ALTER TABLE `TemperatureHistory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_tempHistory_arduino` (`arduino`);

--
-- Index pour la table `WateringHistory`
--
ALTER TABLE `WateringHistory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_waterHistory_arduino` (`arduino`),
  ADD KEY `fk_waterHistory_temp` (`temperature`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `ArduinoController`
--
ALTER TABLE `ArduinoController`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `TemperatureHistory`
--
ALTER TABLE `TemperatureHistory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `WateringHistory`
--
ALTER TABLE `WateringHistory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `TemperatureHistory`
--
ALTER TABLE `TemperatureHistory`
  ADD CONSTRAINT `fk_tempHistory_arduino` FOREIGN KEY (`arduino`) REFERENCES `ArduinoController` (`id`);

--
-- Contraintes pour la table `WateringHistory`
--
ALTER TABLE `WateringHistory`
  ADD CONSTRAINT `fk_waterHistory_arduino` FOREIGN KEY (`arduino`) REFERENCES `ArduinoController` (`id`),
  ADD CONSTRAINT `fk_waterHistory_temp` FOREIGN KEY (`temperature`) REFERENCES `TemperatureHistory` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
