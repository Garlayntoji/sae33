# SAÉ 33 - Station d'arrosage avec relevé de température
## Introduction
Avant de commencer ce README, je tiens à préciser que la plupart des fonctionnalités prévues ne fonctionnent pas ou ne sont pas implémentées. Le manque de temps par rapport à la période des fêtes et aux examens ne m'a pas permis de travailler autant sur le projet que je l'aurais voulu.

## Ce qui marche
- Démarrage d'un websocket sur le serveur, sans faire autre chose de spécifique
- Lecture du port série /dev/ttyACM0 (Linux)
- Relevé de la température avec un script Python

## Ce qui ne marche pas/n'a pas été implémenté
- Appli Android se connectant au serveur pour récupérer les températures
- Envoi d'un signal à l'Arduino pour allumer une LED
- Enregistrement d'une nouvelle carte Arduino dans le système
- Stockage des arrosages en base de données SQL

## Ce qui n'a pas été testé
- Stockage des relevés en base de données SQL

## Initialiser le projet
### Partie matérielle
- Sur un "Shifting-Out' (carte de branchement de composants), brancher en série une résistance 220Ω et une LED bleue. L'électricité doit venir du Digital output, Pin n°8.
- Sur une autre partie de la planche, brancher un capteur de température TMP36 seul, alimenté par le Pin 5V de l'Arduino. On fera passer un câble de données partant de la broche centrale du capteur jusqu'à l'Analog Input A1.

### Partie logicielle
- Décompresser l'archive zip.
- Dans Arduino IDE, uploader le code temperature.ino dans l'Arduino branchée.
- Ouvrir le projet Maven "sae33Java" dans Eclipse.
- Remplacer toute mention du port série "/dev/ttyACM0" par le port série utilisé par l'Arduino.
- Ouvrir votre éditeur de Python préféré, sur saeSerial/serialReader.py.
- Remplacer toute mention du port série "/dev/ttyACM0" par le port série utilisé par l'Arduino.
- Exécuter ServerMaster.java dans Eclipse.

## Android
Bien qu'aucune connexion ne se fasse réellement au serveur, il y a eu une ébauche sur comment l'application pourrait fonctionner. Cela ne reste qu'une tentative. Pour la lancer, ouvrir "MyApplication" dans Android Studio et lancer l'application sur l'émulateur Android ou un terminal physique.