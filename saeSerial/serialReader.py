import serial

# Configuration de la liaison série
port_name = '/dev/ttyACM0'  # Remplacez par le nom de votre port série (p. ex., 'COM1' sous Windows)
baud_rate = 9600

# Ouvrir la liaison série
# ser = serial.Serial(port_name, baud_rate)
ser = serial.Serial()
ser.port = port_name
ser.baudrate = baud_rate

'''
try:
    ser.open()
    while True:
        # Lire une ligne depuis la liaison série
        line = ser.readline().decode('utf-8').strip()
        
        # Afficher les données lues
        print(f'Données lues : {line}')
        
        with open("valeur_lue.txt", "w") as f:
            f.write(line)
'''
try:
    line = ser.readline().decode('utf-8').strip()
    print(line)

except KeyboardInterrupt:
    # Arrêt du programme avec Ctrl+C
    print('Arrêt du programme.')

finally:
    # Fermer la liaison série
    ser.close()
