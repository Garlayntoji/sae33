import serial.tools.list_ports

ports = serial.tools.list_ports.comports()
portsLen = len(ports)

if portsLen > 0:
    for port in ports:
        try:
            ser = serial.Serial(port.device)
            ser.close()
            print(port.device)
        except serial.SerialException:
            print(serial.SerialException)